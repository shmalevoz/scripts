# Скрипты обслуживания исходных кодов конфигураций 1С

### [makesync](https://gitlab.com/shmalevoz/scripts/-/tree/master/makesync)

Создание выполняемого скрипта выгрузки исходных кодов конфигурации 1С 8.2, 8.3 в репозиторий gitlab

Требует наличия установленных git, onescript.

Справку по использованию можно получить запустив любой скрипт без параметров

### Известные проблемы 

В случае установленной x32 1С на x64 linux системах в логе синхронизации наблюдается ошибка

```GLib-GIO-Message: <time>: Using the 'memory' GSettings backend.  Your settings will not be saved or shared with other applications.```

На работоспособбность *не* влияет. Но мозолит глаза в логах.

Устранение: установить i386 пакет dconf-gsettings-backend, в deb системах
```sudo apt-get install dconf-gsettings-backend:i386```
