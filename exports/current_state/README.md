# Выгрузка текущего состояния конфигураций разработки

Зачастую необходимо контролировать состояние разработки в момент **до** помещения изменений в
хранилище. 

Скрипт выполняет выгрузку *текущего* состояния конфигурации и/или расширений в Git путем
создания копий информационных баз и дальнейшей их выгрузки в репозитории Git в отдельные ветки  
Состав задач и используемых ресурсов задается в отдельеых файлах настроек

## Файлы настроек

Форматы файлов настроек, [ссылка](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/scripts/-/raw/master/exports/current_state/formats/config_files_formats.json)

Параметр скрипта | Назначение файла настроек | Ссылка на формат файла 
-----|-----|-----
--resources | Используемые ресурсы - Информационные базы, сервера Git и SQL, ... | [Формат](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/scripts/-/raw/master/exports/current_state/formats/config_files_formats.json#/operations/get-resources)
--auth | Параметры авторизации на ресурсах | [Формат](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/scripts/-/raw/master/exports/current_state/formats/config_files_formats.json#/operations/get-auth)
--platforms | Используемые платформы 1С | [Формат](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/scripts/-/raw/master/exports/current_state/formats/config_files_formats.json#/operations/get-platforms)
--tasks | Задачи выгрузки | [Формат](https://elements-demo.stoplight.io/?spec=https://gitlab.com/shmalevoz/scripts/-/raw/master/exports/current_state/formats/config_files_formats.json#/operations/get-tasks)
