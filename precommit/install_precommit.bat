@echo off
SetLocal EnableExtensions EnableDelayedExpansion

set RETURNCODE=0
set ERRORTEXT=
set GITPATH=
set SCRIPTNAME=%~n0
set SCRIPTPATH=%~sdp0

call :get_exec_full_path git.exe
if "!RESULT!"=="" (
	set ERRORTEXT="�� ������ git.exe!"
) else (
	call :get_path "!RESULT!"
	set GITPATH=!RESULT!
)

rem �����ࠥ� ��㬥��� ��������� ��ப�
:loop_parse_args_not_empty
if "%1"=="" goto loop_parse_args_end

	call :parse_args %1 %2
	shift
	goto :loop_parse_args_not_empty
:loop_parse_args_end

if "!repopath!"=="" (
	set ERRORTEXT="�� 㪠��� ��ࠬ��� ��⠫��� ९������ Git repopath!"
)

if "!ERRORTEXT!"=="" (
	call :install_hook "!GITPATH!" "!SCRIPTPATH!" "!REPOPATH!" "!SCRIPTNAME!"
	call :print_string "��⠭���� �����襭�."
) else (
	call :print_help "!SCRIPTNAME!"
)

:script_end
exit /b !RETURNCODE!

rem ---------------------------------------------------
rem ��⠭���� ���墠�稪�
rem %1 - ��⠫�� git.exe
rem %2 - ��⠫�� �ਯ�
rem %3 - ��⠫�� ९������
rem %4 - ��� �ਯ�
:install_hook

	set PATH=%PATH%;%~1..\usr\bin;%~1..\mingw\bin;%~1..\mingw64\bin

    curl https://gitlab.com/shmalevoz/scripts/raw/master/precommit/v8cf.jar --output "%~3\.git\hooks\v8cf.jar"
    curl https://gitlab.com/shmalevoz/scripts/raw/master/precommit/pre-commit --output "%~3\.git\hooks\pre-commit"
    chmod a+x "%~3\.git\hooks\pre-commit"

exit /b
rem install_hook
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��� �६������ 䠩��(��⠫���)
rem %1 - ��� �ਯ�
:get_temp_name

	:get_next_temp_name

	set _name="%TEMP%\%~1.%random%.tmp"
	if exist !_name! goto :get_next_temp_name

	set RESULT=!_name!

exit /b
rem get_temp_name
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ����� �ࠢ��
rem %1 - ��� �ਯ�
:print_help

	call :print_string "��ਯ� ��⠭���� ����ᯠ����� 䠩��� ���譨� ���⮢(��ࠡ�⮪)"
	call :print_string "�� �� ����饭�� � Git ९����਩"
	call :print_string "�ॡ�� ��⠭��������� (����㯭��� �� �믮������ �� ��� ��⠫���) git.exe"
	call :print_string "�ᯮ�짮�����:"
	call :print_string "%~1 --repopath path"

	if not "!ERRORTEXT!"=="" (
		call :print_string "�訡��:"
		call :print_string !ERRORTEXT!
	)

exit /b
rem print_help
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� � RETURN ����� ���� � �ᯮ��塞��� 䠩��
rem %1 - ��� �ᯮ��塞��� 䠩��
:get_exec_full_path

	set RESULT=%~$PATH:1

exit /b
rem get_exec_full_path
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ����� ��⠫�� ��� ��।������ ��㬥��
rem %1 - ���� � 䠩��
:get_path

	set RESULT=%~sdp1

exit /b
rem get_path
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �뢮��� �� ��࠭ ��ப� �᪫��� ��ࠬ���騥 ����窨
rem %1 - ���⠥��� ��ப�
:print_string

	echo %~1

exit /b
rem print_string
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ������ ��㬥�⮢ ��������� ��ப�
rem %1 - ��㬥��� ��������� ��ப�
rem %2 
:parse_args

	set prefix=%1
	set prefix=%prefix:~0,2%
	if "%prefix%"=="--" (
		for /f "tokens=1* delims=--" %%a in ("%1") do (
			set %%a=%2
		)
	)

exit /b
rem parse_args
rem ---------------------------------------------------
