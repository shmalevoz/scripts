#!/bin/sh

# Печать справки по использования
# $1 - имя скрипта
# $2 - текст ошибки
print_help() {
    echo "Установка автоматической распаковки файлов внешних отчетов(обработок) 1С предприятия"
    echo "при их помещении под контроль версий Git"
    echo "Использование: "
    echo "$1 --repopath <Каталог репозитория Git> [--scriptname <Имя файла скрипта>]"
    if [ -n "$2" ]; then
	echo
	echo "Ошибка:"
	echo $2
    fi
}

# Установка необходимых файлов
# $1 - каталог репозитория
install_hook() {
    curl https://gitlab.com/shmalevoz/scripts/raw/master/precommit/v8cf.jar --output "$1/.git/hooks/v8cf.jar"
    curl https://gitlab.com/shmalevoz/scripts/raw/master/precommit/pre-commit --output "$1/.git/hooks/pre-commit"
    curl https://gitlab.com/shmalevoz/scripts/raw/master/precommit/post-commit --output "$1/.git/hooks/post-commit"
    chmod a+x "$1/.git/hooks/pre-commit"
    chmod a+x "$1/.git/hooks/post-commit"
}

# значения переданнх ключей
REPOPATH=
ERRORTEXT=
SCRIPTNAME=$0

while [ -n "$1" ]; do
    case "$1" in
	--repopath) REPOPATH=$2
	    shift;;
	--scriptname) SCRIPTNAME=$2
	    shift;;
	*)
    esac
    shift
done

# Проверяем корректность переданных параметров
if ! [ -d "$REPOPATH/.git" ]; then
    ERRORTEXT="Указанный каталог не является корневым в Git репозитории"
fi
if ! [ -d "$REPOPATH" ]; then
    ERRORTEXT="Не существует каталога $REPOPATH"
fi
if [ -z "$REPOPATH" ]; then
    ERRORTEXT="Не указан каталог репозитория Git"
fi
if [ -n "$ERRORTEXT" ]; then
    print_help "$SCRIPTNAME" "$ERRORTEXT"
else
    install_hook "$REPOPATH"
fi
