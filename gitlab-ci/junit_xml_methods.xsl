<?xml version="1.0" encoding="utf-8"?>
<!-- 
Вывод имен объектов и их методов, участвовавших в тесте
Valeriy Krynin <shmalevoz@gmail.com> 2021
MIT Licence
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" indent="no"/>
	<xsl:strip-space elements="*"/>

	<!-- статусы
		passed - успешно пройден
		failure - тестовое условие не выполнено
		error - ошибка времени выполнения (код породил исключение)
		skipped - пропущен объявленный тест (не найден метод в контейнере)
	-->

	<!-- предварительные объявления -->
	<!-- ОС определяем по вендору. libxslt считаем unix системами, остальное windows -->
	<xsl:variable name="vendor" select="system-property('xsl:vendor')"/>
	<xsl:variable name="unix_vendor" select="'libxslt'"/>
	<xsl:variable name="crlf">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\n</xsl:when>
			<xsl:otherwise>&#10;</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- основное преобразование -->
	<xsl:template match="/testsuites">
		<xsl:apply-templates select="testsuite">
			<xsl:with-param name="suite" select="''"/>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="testsuite">
		<xsl:param name="suite"/>

		<xsl:variable name="depth" select="count(ancestor::*)"/>

		<!-- на первом уровне каталог, на втором должен быть контейнер. группы пропускаем -->
		<xsl:choose>
			<xsl:when test="$depth = 2">
				<xsl:apply-templates select="testsuite">
					<xsl:with-param name="suite" select="@name"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="testcase">
					<xsl:with-param name="suite" select="@name"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="testsuite">
					<xsl:with-param name="suite" select="$suite"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="testcase">
					<xsl:with-param name="suite" select="$suite"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="testcase">
		<xsl:param name="suite"/>

		<xsl:if test="not(@status = 'skipped')">
			<xsl:value-of select="$suite"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="@name"/>
			<xsl:value-of select="$crlf"/>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>