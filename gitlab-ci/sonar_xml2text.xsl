<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>

    <!-- ОС определяем по вендору. libxslt считаем unix системами, остальное windows -->
    <xsl:variable name="vendor" select="system-property('xsl:vendor')"/>
    <xsl:variable name="unix_vendor" select="'libxslt'"/>
    <xsl:variable name="crlf">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\n</xsl:when>
            <xsl:otherwise>&#10;</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <!-- цвета символов при раскраске вывода, windows лишен такой плюшки -->
    <xsl:variable name="font_black">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[90m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_red">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[91m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_green">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[92m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_yellow">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[93m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_blue">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[94m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_purple">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[95m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_cyan">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[96m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="font_white">
        <xsl:choose>
            <xsl:when test="$vendor = $unix_vendor">\e[97m</xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/map">
        <xsl:apply-templates select="array[@key = 'issues']"/>
    </xsl:template>

    <xsl:template match="array[@key = 'issues']">
		<xsl:variable name="cnt" select="count(./map)"/>
		<xsl:choose>
			<xsl:when test="not($cnt = 0)">
				<xsl:text>Дефекты кода:</xsl:text>
				<xsl:value-of select="$crlf"/>
				<xsl:apply-templates select="map"/>
			</xsl:when>
		</xsl:choose>
    </xsl:template>

    <xsl:template match="map">

        <xsl:variable name="project" select="normalize-space(./string[@key = 'project'])"/>
        <xsl:variable name="component" select="normalize-space(./string[@key = 'component'])"/>
        <xsl:variable name="severity" select="normalize-space(./string[@key = 'severity'])"/>
        <xsl:variable name="message" select="normalize-space(./string[@key = 'message'])"/>
        <xsl:variable name="line" select="normalize-space(./number[@key = 'line'])"/>
        <xsl:variable name="rule" select="substring(normalize-space(./string[@key = 'rule']), 21)"/>
        <xsl:variable name="module" select="substring($component, string-length($project) + 2)"/>

        <xsl:value-of select="$font_blue"/>
        <xsl:text>:: </xsl:text>
        <xsl:value-of select="$message"/>
        <xsl:value-of select="$crlf"/>
        <xsl:value-of select="$font_white"/>
        <xsl:text>SOURCE -> </xsl:text>
		<xsl:text>$BASEURL/</xsl:text>
        <xsl:value-of select="$module"/>
        <xsl:text>#L</xsl:text>
        <xsl:value-of select="$line"/>
        <xsl:value-of select="$crlf"/>
        <xsl:value-of select="$font_red"/>
        <xsl:value-of select="$severity"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$font_white"/>
        <xsl:value-of select="$rule"/>
        <xsl:text> -> https://1c-syntax.github.io/bsl-language-server/dev/diagnostics/</xsl:text>
        <xsl:value-of select="$rule"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$crlf"/>


    </xsl:template>

</xsl:stylesheet>