<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" indent="no"/>
	<xsl:strip-space elements="*"/>

	<!-- статусы
		passed - успешно пройден
		failure - тестовое условие не выполнено
		error - ошибка времени выполнения (код породил исключение)
		skipped - пропущен объявленный тест (не найден метод в контейнере)
	-->

	<!-- предварительные объявления -->
	<!-- ОС определяем по вендору. libxslt считаем unix системами, остальное windows -->
	<xsl:variable name="vendor" select="system-property('xsl:vendor')"/>
	<xsl:variable name="unix_vendor" select="'libxslt'"/>
	<xsl:variable name="crlf">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\n</xsl:when>
			<xsl:otherwise>&#10;</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- цвета символов при раскраске вывода, windows лишен такой плюшки -->
	<xsl:variable name="font_black">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[90m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_red">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[91m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_green">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[92m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_yellow">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[93m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_blue">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[94m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_purple">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[95m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_cyan">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[96m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="font_white">
		<xsl:choose>
			<xsl:when test="$vendor = $unix_vendor">\e[97m</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<!-- основное преобразование -->
	<xsl:template match="/testsuites">
		<xsl:call-template name="group_info">
			<xsl:with-param name="count" select="@tests"/>
			<xsl:with-param name="failed" select="@failures"/>
			<xsl:with-param name="errors" select="@errors"/>
			<xsl:with-param name="skipped" select="@skipped"/>
			<xsl:with-param name="time" select="@time"/>
		</xsl:call-template>
		<xsl:apply-templates select="testsuite"/>
	</xsl:template>

	<xsl:template match="testsuite">
		<xsl:variable name="depth" select="count(ancestor::*)"/>
		<xsl:variable name="count" select="count(.//testcase)"/>
		<xsl:variable name="passed" select="count(.//testcase[@status = 'passed'])"/>
		<xsl:variable name="failed" select="count(.//testcase[@status = 'failure'])"/>
		<xsl:variable name="errors" select="count(.//testcase[@status = 'error'])"/>
		<xsl:variable name="skipped" select="count(.//testcase[@status = 'skipped'])"/>
		<xsl:variable name="time" select="sum(.//testcase/@time)"/>
		<xsl:variable name="has_error" select="$failed + $errors + $skipped"/>
		<!-- выводим только ошибки, не спамим успехами -->
		<xsl:if test="not($has_error = '0')">
			<!-- выводим информацию исключая корневой каталог, он дублирует общую нформацию -->
			<xsl:if test="$depth &gt; 1">
				<xsl:value-of select="$crlf"/>
				<xsl:call-template name="indent"/>
				<xsl:value-of select="$font_yellow"/>
				<xsl:value-of select="@name"/>
				<xsl:value-of select="$font_white"/>
				<xsl:text> </xsl:text>
				<xsl:call-template name="group_info">
					<xsl:with-param name="count" select="$count"/>
					<xsl:with-param name="failed" select="$failed"/>
					<xsl:with-param name="errors" select="$errors"/>
					<xsl:with-param name="skipped" select="$skipped"/>
					<xsl:with-param name="time" select="$time"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:apply-templates select="testsuite"/>
			<xsl:apply-templates select="testcase"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="testcase">
		<xsl:choose>
			<xsl:when test="@status = 'error'">
				<xsl:apply-templates select="error"/>
			</xsl:when>
			<xsl:when test="@status = 'failure'">
				<xsl:apply-templates select="failure"/>
			</xsl:when>
			<xsl:when test="@status = 'skipped'">
				<xsl:apply-templates select="skipped"/>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="error">
		<xsl:call-template name="case_error_message">
			<xsl:with-param name="case_name" select="../@name"/>
			<xsl:with-param name="status" select="'Ошибка'"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="failure">
		<xsl:call-template name="case_error_message">
			<xsl:with-param name="case_name" select="../@name"/>
			<xsl:with-param name="status" select="'Условие не выполнено'"/>
			<!-- надеемся, что в тесте не встретится сравнение строк имеющих подстроку ВызватьИсключение. если будет, то срежет часть информации -->
			<xsl:with-param name="message" select="substring-before(substring-after(@message, '[Failed] '), 'ВызватьИсключение ')"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="skipped">
		<xsl:call-template name="case_error_message">
			<xsl:with-param name="case_name" select="../@name"/>
			<xsl:with-param name="status" select="'Пропущено'"/>
			<xsl:with-param name="message" select="@message"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="group_info">
		<xsl:param name="count"/>
		<xsl:param name="failed"/>
		<xsl:param name="errors"/>
		<xsl:param name="skipped"/>
		<xsl:param name="time"/>

		<xsl:variable name="success" select="$count - $failed - $errors - $skipped"/>

		<xsl:value-of select="$font_white"/>
		<xsl:text>Всего тестов: </xsl:text>
		<xsl:if test="$count = $success"><xsl:value-of select="$font_green"/></xsl:if>
		<xsl:value-of select="$count"/>
		<xsl:if test="$count = $success"><xsl:value-of select="$font_white"/></xsl:if>
		<xsl:text>, успешно: </xsl:text>
		<xsl:if test="$count = $success"><xsl:value-of select="$font_green"/></xsl:if>
		<xsl:value-of select="$success"/>
		<xsl:if test="$count = $success"><xsl:value-of select="$font_white"/></xsl:if>
		<!-- failure -->
		<xsl:call-template name="cases_status">
			<xsl:with-param name="name" select="'условий не выполнено'"/>
			<xsl:with-param name="count" select="$failed"/>
		</xsl:call-template>
		<!-- errors -->
		<xsl:call-template name="cases_status">
			<xsl:with-param name="name" select="'ошибок'"/>
			<xsl:with-param name="count" select="$errors"/>
		</xsl:call-template>
		<!-- skipped -->
		<xsl:call-template name="cases_status">
			<xsl:with-param name="name" select="'пропущено'"/>
			<xsl:with-param name="count" select="$skipped"/>
		</xsl:call-template>
		<!-- time elapsed -->
		<xsl:text>, время выполнения: </xsl:text>
		<xsl:value-of select="format-number($time, '#0.000')"/>
		<xsl:text> секунд(ы)</xsl:text>
	</xsl:template>

	<xsl:template name="case_error_message">
		<xsl:param name="case_name"/>
		<xsl:param name="status"/>
		<xsl:param name="message"/>

		<xsl:value-of select="$crlf"/>
		<xsl:call-template name="indent"/>
		<xsl:value-of select="$font_red"/>
		<xsl:text>[</xsl:text>
		<xsl:value-of select="$status"/>
		<xsl:text>] </xsl:text>
		<xsl:value-of select="$font_yellow"/>
		<xsl:value-of select="$case_name"/>
		<xsl:value-of select="$font_white"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$message"/>
	</xsl:template>

	<xsl:template name="cases_status">
		<xsl:param name="name"/>
		<xsl:param name="count"/>

		<xsl:text>, </xsl:text>
		<xsl:if test="not($count = '0')"><xsl:value-of select="$font_red"/></xsl:if>
		<xsl:value-of select="$name"/>
		<xsl:text>: </xsl:text>
		<xsl:value-of select="$count"/>
		<xsl:if test="not($count = '0')"><xsl:value-of select="$font_white"/></xsl:if>
	</xsl:template>

	<xsl:template name="indent">
		<xsl:call-template name="indent_print">
			<xsl:with-param name="index" select="1"/>
			<xsl:with-param name="count" select="count(ancestor::*) - 1"/>
		</xsl:call-template>
	</xsl:template>


	<xsl:template name="indent_print">
		<xsl:param name="index"/>
		<xsl:param name="count"/>

		<xsl:if test="$index &lt;= $count">
			<xsl:text>  </xsl:text>
			<xsl:call-template name="indent_print">
				<xsl:with-param name="index" select="$index + 1"/>
				<xsl:with-param name="count" select="$count"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>