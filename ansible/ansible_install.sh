#!/usr/bin/env bash

# Скрипт установки Ansible на дистрибутивах семейств Debian и Redhat
# 
# Выполняет
#   * обновляет систему
#   * заводит служебного пользователя _ansible
#   * генерирует к нему ключи доступа в ~/.ssh/local_ansible
#   * генерирует и устанавливает надежный пароль для _ansible
#
# на выходе имеем установленный ansible и доступ к служебному пользователю
# _ansible
# по ssh для текущего пользователя системы

# Необходимы root привилегии
if [[ "$(id -u)" -ne 0 ]]; then echo "Для выполнения скрипта необходимы root привилегии"; exit 1; fi

# Возвращает строку в нижнем регистре
lowercase(){
    echo "$1" | awk '{print tolower($0)}'
}

# Возвращает в переменных информацию о дистрибутиве
#	OS - windows|mac|solaris|aix|linux
#	DistroBasedOn - redhat|suse|mandrake|debian для OS==linux
#	DIST - имя дистрибутива
function set_distro_vars() {

	OS=$(lowercase "$(uname)")
	DIST=""
	DistroBasedOn=""

	if [ "{$OS}" = "windowsnt" ]; then
		OS=windows
	elif [ "{$OS}" = "darwin" ]; then
		OS=mac
	else
		OS=$(uname)
		if [ "${OS}" = "SunOS" ]; then
			OS=Solaris
		elif [ "${OS}" = "AIX" ]; then
			OS="$OS"
		elif [ "${OS}" = "Linux" ]; then
			if [ -f /etc/redhat-release ] ; then
				DistroBasedOn='RedHat'
				DIST=$(cat /etc/redhat-release | sed 's/\ release.*//')
			elif [ -f /etc/SuSE-release ] ; then
				DistroBasedOn='SuSe'
			elif [ -f /etc/mandrake-release ]; then
				DistroBasedOn='Mandrake'
			elif [ -f /etc/debian_version ]; then
				DistroBasedOn='Debian'
				DIST=$(cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }')
			fi
			if [ -f /etc/UnitedLinux-release ] ; then
				DIST="${DIST}[$(cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//)]"
			fi
			OS=$(lowercase $OS)
			DistroBasedOn=$(lowercase $DistroBasedOn)
			readonly OS
			readonly DIST
			readonly DistroBasedOn
		fi

	fi
}

# Поддерживаем ТОЛЬКО debian и redhat дистрибутивы
set_distro_vars

case "$DistroBasedOn" in
	debian)
		apt update 
		apt upgrade -y
		apt install -y ansible
		;;
	redhat)
		# добавляем EPEL реаозиторий
		yum install -y epel-release
		yum update -y
		yum install -y ansible
		;;
	*)
		echo "Неподдерживаемый дистрибутив! Скрипт поддерживает только дистрибутивы семейств Debian и Redhat"
		exit 1
		;;
esac

# добавляем служебного пользователя
useradd -d /home/_ansible -s /bin/bash _ansible
# Позволяем служебному пользователю привилегированный режим
echo "# Allow ansible root commands" >> /etc/sudoers
echo "_ansible ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers
# И назначаем ему надежный пароль
USER_PWD=$(openssl rand -base64 32)
echo -e "$USER_PWD\n$USER_PWD" | passwd _ansible

# Даем доступ к _ansible для текушего пользователя 
USER_HOME=$(getent passwd $SUDO_USER | cut -d: -f6)
USER_GROUP=$(id -gn $SUDO_USER)
mkdir -p "$USER_HOME/.ssh"

# Генерируем ключи и добавляем ключ для доступа текушему пользователю
mkdir -p /home/_ansible/.ssh
ssh-keygen -t rsa -b 3072 -N "" -C '_ansible@localhost' -f "$USER_HOME/.ssh/local_ansible" -q
touch /home/_ansible/.ssh/authorized_keys
cat $USER_HOME/.ssh/local_ansible.pub >> /home/_ansible/.ssh/authorized_keys

# Добавляем текущую машину в список известных хостов ssh для текущего пользователя
ssh-keyscan -H 127.0.0.1 >> "$USER_HOME/.ssh/known_hosts"
chown -R $SUDO_USER:$USER_GROUP "$USER_HOME/.ssh"

# Назначаем права для каталогов _ansible
chown -R _ansible:_ansible /home/_ansible
chmod -R 0700 /home/_ansible

# Запрещаем вход по ssh с паролем для _ansible
echo "Match User _ansible" >> /etc/ssh/sshd_config
echo "  PasswordAuthentication no" >> /etc/ssh/sshd_config

# Формируем список управляемых хостов для текущего пользователя
A_HOME="$USER_HOME/ansible"
A_INVENTORY="$A_HOME/inventory"
A_HOSTS="$A_INVENTORY/local.yml"

mkdir -p "$A_INVENTORY"
touch "$A_HOSTS"
echo "local:" > "$A_HOSTS"
echo "  hosts:" >> "$A_HOSTS"
echo "    127.0.0.1" >> "$A_HOSTS"

chown -R $SUDO_USER:$USER_GROUP "$A_HOME"
chown -R $SUDO_USER:$USER_GROUP "$USER_HOME/.ssh"
