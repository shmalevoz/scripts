# Установка локально управляемого Ansible

> Текущий пользователь должен обладать правами ```sudo```

Будет выполнено

* Сервер обновлен
* Создан служебный пользователь ```_ansible``` с правами sudo, без возможности интерактивной авторизации
* Установлен Ansible
* Для текущего пользователя предоставлен доступ по ssh к пользователю _ansible с помощью ключа авторизации. Ключ будет сохранен в ~/.ssh/local_ansible
* Настройки со списком управляемых хостов будут сохранены в 

> ВАЖНО! Инструкция написана для использования Ansible ***на локальной машине***. Для удаленного использования необходимо скопировать ключ доступа ~/.ssh/local_ansible на машину управления и использовать его в командах.

## Установка

Скачиваем и запускаем скрипт установки

```bash
curl -s https://gitlab.com/shmalevoz/scripts/-/raw/master/ansible/ansible_install.sh?inline=false | sudo bash
```

* Вводим пароль пользователя
* Ждем
* Перезагружаем машину

Система обновлена, Ansible установлен и для текущего пользователя есть к нему доступ по ssh

Проверяем, что все работает

```bash
ansible local -i ~/ansible/inventory --private-key ~/.ssh/local_ansible -u _ansible -m ping
```

должен быть вывод

```bash
127.0.0.1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```

## Использование

Ansible готов к использованию с применением ключа авторизации, размещенного в ~/.ssh/local_ansible пользователя, использованного при установке

Пример для локального использования:

```bash
ansible local \
  --inventory ~/ansible/inventory \
  --private-key ~/.ssh/local_ansible \
  --user _ansible \
  playbooks/install-ci.yml
```
