#!/usr/bin/env bash

# Скрипт запуска установки Gitlab через ansible
#
# Valeriy Krynin <shmalevoz@gmail.com> 2024
# MIT License
#
# Примеры настроек SMTP см. на
# https://docs.gitlab.com/omnibus/settings/smtp.html

#
# методы
#

# Вывод $1 различными цветами
print_red() {
    echo -en "\e[91m$1" 1>&2
}
print_green() {
    echo -en "\e[92m$1" 1>&2
}
print_yellow() {
    echo -en "\e[93m$1" 1>&2
}
print_blue() {
    echo -en "\e[94m$1" 1>&2
}
print_magenta() {
    echo -en "\e[95m$1" 1>&2
}
print_cyan() {
    echo -en "\e[96m$1" 1>&2
}
print_white() {
    echo -en "\e[97m$1" 1>&2
}
set_def_colors() {
    print_white ""
}
print_crlf() {
    set_def_colors
    echo "" 1>&2
}

# Печать справки по опции скрипта
# $1 - вводимое значение
# $2 - справка
print_help_opt() {
    echo "	$1"
    echo "		$2"
}

# Печать справки
# $1 - имя скрипта
print_help() {
    echo "Скрипт установки Gitlab через Ansible"
    echo "  Требует наличия playbook pb-gitlab.yml установки из https://gitlab.com/shmalevoz/scripts/-/tree/master/ansible/playbooks"
    echo ""
    echo "Использование"
    echo "$1 -h HOST -k PRIVATE_KEY_FILE [--опция значение [--опция значение...]]"
    echo "Параметры:"
    print_help_opt "-h HOST" "Имя или IP адрес хоста назначения"
    print_help_opt "-k PRIVATE_KEY_FILE" "Путь к файлу приватного ключа авторизации по ssh на целевом хосте"
    echo "Опции, необязательные:"
    # Опции Ansible
    echo "  Параметры выполнения Ansible"
    print_help_opt "--user USERNAME" "Пользователь Ansible под которым будут выполняться задачи. По-умолчанию _ansible"
    print_help_opt "--playbook-dir PATH" "Каталог задач Ansible (playbooks). По-умолчанию подкаталог playbooks скрипта"
    # Опции Gitlab
    echo "  Основные настройки установки Gitlab"
    echo "    Примеры настроек для различных почтовых серверов см. на https://docs.gitlab.com/omnibus/settings/smtp.html"
    print_help_opt "--gitlab_domain ADDRESS" "Адрес доступа к Gitlab. Умолчание 'gitlab.local'"
    print_help_opt "--gitlab_redirect_http_to_https true|false" "Включить перенаправление http на https. Умолчание false"
    print_help_opt "--gitlab_email_enabled true|false" "Флаг включения рассылок по электронной почте. Умолчание false"
    print_help_opt "--gitlab_email_from EMAIL" "Адрес отправителя писем. Умолчание 'gitlab@example.com'"
    print_help_opt "--gitlab_email_display_name NAME" "Отображаемое имя исполнителя. Умолчание 'Gitlab'"
    print_help_opt "--gitlab_email_reply_to EMAIL" "Адрес ответного письма. Умолчание адрес gitlab_email_from"
    print_help_opt "--gitlab_smtp_enable true:false" "Включена отправка писем. Умолчание false"
    print_help_opt "--gitlab_smtp_address URI" "Адрес SMTP сервера. Умолчание 'smtp.server'"
    print_help_opt "--gitlab_smtp_port NUMBER" "Порт SMTP сервера. Умолчание 465"
    print_help_opt "--gitlab_smtp_user_name NAME" "Пользователь доступа к SMTP серверу. Умолчание 'smtp_user'"
    print_help_opt "--gitlab_smtp_password PASS" "Пароль доступа к SMTP серверу. Умолчание 'smtp_password'"
    print_help_opt "--gitlab_smtp_domain" "Домен SMTP. Умолчание 'example.com'"
    print_help_opt "--gitlab_smtp_authentication login|plain" "Тип авторизации SMTP. Умолчание 'login'"
    print_help_opt "--gitlab_smtp_enable_starttls_auto true|false" "Автоматически использовать TLS. Умолчание false"
    print_help_opt "--gitlab_smtp_tls true|false" "Использовать TLS. Умолчание true"
    print_help_opt "--gitlab_smtp_openssl_verify_mode none|peer|client_once|fail_if_no_peer_cert" "Вариант использования SSL. Умолчание peer"
    # Опции скрипта
    echo "  Параметры скрипта"
    print_help_opt "--debug" "Вывод отладочной информации"
    print_help_opt "--help" "Вывод этого экрана справки"
}

# Вывод отладочного сообщения
# $1 - Текст
print_debug() {
    if [[ "$DEBUG" == "1" ]]; then
        echo "DEBUG: $1" 1>&2
    fi
}

BASEDIR=$(dirname "$0")
SCRIPTDIR=$(
    cd "$BASEDIR" || exit
    pwd
)
SCRIPTNAME=$(basename "$0")
ARGSCOUNT=$#
ERRORTEXT=""
EXITCODE=0

HOSTNAME=""
HOSTUSR="_ansible"
PLAYBOOKDIR=""
KEYFILE=""
DEBUG="0"
PRINTHELP="0"
TASKS=""

g_domain="gitlab.local"
g_redirect_http_to_https="false"
g_email_enabled="false"
g_email_from="gitlab@example.com"
g_email_display_name="Gitlab"
g_email_reply_to="$g_email_from"
g_smtp_enable="false"
g_smtp_address="smtp.server"
g_smtp_port="465"
g_smtp_user_name="smtp_user"
g_smtp_password="smtp_password"
g_smtp_domain="example.com"
g_smtp_authentication="login"
g_smtp_enable_starttls_auto="false"
g_smtp_tls="true"
g_smtp_openssl_verify_mode="peer"


while [[ -n "$1" ]]; do
    case "$1" in
    -h)
        HOSTNAME="$2"
        shift
        ;;
    -k)
        KEYFILE="$2"
        shift
        ;;
    --user)
        HOSTUSR="$2"
        shift
        ;;
    --playbook-dir)
        PLAYBOOKDIR="$2"
        shift
        ;;
    --debug)
        DEBUG="1"
        ;;
    --help)
        PRINTHELP="1"
        ;;
    # Gitlab
    --gitlab_domain)
        g_domain="$2"
        shift
        ;;
    --gitlab_redirect_http_to_https)
        g_redirect_http_to_https="$2"
        shift
        ;;
    --gitlab_email_enabled)
        g_email_enabled="$2"
        shift
        ;;
    --gitlab_email_from)
        g_email_from="$2"
        shift
        ;;
    --gitlab_email_display_name)
        g_email_display_name="$2"
        shift
        ;;
    --gitlab_email_reply_to)
        g_email_reply_to="$2"
        shift
        ;;
    --gitlab_smtp_enable)
        g_smtp_enable="$2"
        shift
        ;;
    --gitlab_smtp_address)
        g_smtp_address="$2"
        shift
        ;;
    --gitlab_smtp_port)
        g_smtp_port="$2"
        shift
        ;;
    --gitlab_smtp_user_name)
        g_smtp_user_name="$2"
        shift
        ;;
    --gitlab_smtp_password)
        g_smtp_password="$2"
        shift
        ;;
    --gitlab_smtp_domain)
        g_smtp_domain="$2"
        shift
        ;;
    --gitlab_smtp_authentication)
        g_smtp_authentication="$2"
        shift
        ;;
    --gitlab_smtp_enable_starttls_auto)
        g_smtp_enable_starttls_auto="$2"
        shift
        ;;
    --gitlab_smtp_tls)
        g_smtp_tls="$2"
        shift
        ;;
    --gitlab_smtp_openssl_verify_mode)
        g_smtp_openssl_verify_mode="$2"
        shift
        ;;
    *)
        ERRORTEXT="Неизвестный параметр $1"
        break
        ;;
    esac
    shift
done

# Проверяем параметры
if [[ -z "$ERRORTEXT" ]]; then
    if [[ -z "$PLAYBOOKDIR" ]]; then
        PLAYBOOKDIR="$SCRIPTDIR/playbooks"
    fi
    TASKS="$PLAYBOOKDIR/pb-gitlab_server.yml"
    if [[ $ARGSCOUNT -eq 0 ]]; then
        PRINTHELP="1"
    fi
    if [[ $ARGSCOUNT -eq 1 && "$PRINTHELP" == "1" ]]; then
        PRINTHELP="1"
    fi
    if [[ -z "$HOSTNAME" ]]; then
        ERRORTEXT="Необходимо задать имя хоста назначения"
    elif [[ -z "$KEYFILE" ]]; then
        ERRORTEXT="Необходимо задать расположение файла приватного ключа авторизации на целевом хосте"
    elif [[ ! -r "$KEYFILE" ]]; then
        ERRORTEXT="Не найден файл приватного ключа авторизации $KEYFILE"
    elif [[ ! -r "$TASKS" ]]; then
        ERRORTEXT="Не найден файл задач Ansible $TASKS"
    fi
fi

# Если все хорошо, то можно вызывать полезную нагрузку
if [[ -z "$ERRORTEXT" ]]; then
    ansible-playbook --inventory "$HOSTNAME," \
        --private-key "$KEYFILE" \
        --user "$HOSTUSR" \
        --extra-vars "env_gitlab_domain=\"$g_domain\"" \
        --extra-vars "env_gitlab_redirect_http_to_https=\"$g_redirect_http_to_https\"" \
        --extra-vars "env_gitlab_email_enabled=\"$g_email_enabled\"" \
        --extra-vars "env_gitlab_email_from=\"$g_email_from\"" \
        --extra-vars "env_gitlab_email_display_name=\"$g_email_display_name\"" \
        --extra-vars "env_gitlab_email_reply_to=\"$g_email_reply_to\"" \
        --extra-vars "env_gitlab_smtp_enable=\"$g_smtp_enable\"" \
        --extra-vars "env_gitlab_smtp_address=\"$g_smtp_address\"" \
        --extra-vars "env_gitlab_smtp_port=\"$g_smtp_port\"" \
        --extra-vars "env_gitlab_smtp_user_name=\"$g_smtp_user_name\"" \
        --extra-vars "env_gitlab_smtp_password=\"$g_smtp_password\"" \
        --extra-vars "env_gitlab_smtp_domain=\"$g_smtp_domain\"" \
        --extra-vars "env_gitlab_smtp_authentication=\"$g_smtp_authentication\"" \
        --extra-vars "env_gitlab_smtp_enable_starttls_auto=\"$g_smtp_enable_starttls_auto\"" \
        --extra-vars "env_gitlab_smtp_tls=\"$g_smtp_tls\"" \
        --extra-vars "env_gitlab_smtp_openssl_verify_mode=\"$g_smtp_openssl_verify_mode\"" \
        "$TASKS"
fi

# Завершение
if [[ "$PRINTHELP" == "1" ]]; then
	print_help "$SCRIPTNAME"
fi
if [[ -n "$ERRORTEXT" ]]; then
	print_red "$ERRORTEXT"
	print_crlf
	EXITCODE=1
fi

exit $EXITCODE
