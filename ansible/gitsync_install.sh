#!/usr/bin/env bash

# Скрипт запуска установки Gitsync через ansible
#
# Valeriy Krynin <shmalevoz@gmail.com> 2024
# MIT License

#
# методы
#

# Вывод $1 различными цветами
print_red() {
    echo -en "\e[91m$1" 1>&2
}
print_green() {
    echo -en "\e[92m$1" 1>&2
}
print_yellow() {
    echo -en "\e[93m$1" 1>&2
}
print_blue() {
    echo -en "\e[94m$1" 1>&2
}
print_magenta() {
    echo -en "\e[95m$1" 1>&2
}
print_cyan() {
    echo -en "\e[96m$1" 1>&2
}
print_white() {
    echo -en "\e[97m$1" 1>&2
}
set_def_colors() {
    print_white ""
}
print_crlf() {
    set_def_colors
    echo "" 1>&2
}

# Печать справки по опции скрипта
# $1 - вводимое значение
# $2 - справка
print_help_opt() {
    echo "	$1"
    echo "		$2"
}

# Печать справки
# $1 - имя скрипта
print_help() {
    echo "Скрипт установки Gitsync через Ansible"
    echo "  Требует наличия playbook pb-gitsync.yml установки из https://gitlab.com/shmalevoz/scripts/-/tree/master/ansible/playbooks"
    echo ""
    echo "Использование"
    echo "$1 -h HOST -k PRIVATE_KEY_FILE [--опция значение [--опция значение...]]"
    echo "Параметры:"
    print_help_opt "-h HOST" "Имя или IP адрес хоста назначения"
    print_help_opt "-k PRIVATE_KEY_FILE" "Путь к файлу приватного ключа авторизации по ssh на целевом хосте"
    echo "Опции, необязательные:"
    print_help_opt "--user USERNAME" "Пользователь Ansible под которым будут выполняться задачи. По-умолчанию _ansible"
    print_help_opt "--playbook-dir PATH" "Каталог задач Ansible (playbooks). По-умолчанию подкаталог playbooks скрипта"
    print_help_opt "--debug" "Вывод отладочной информации"
    print_help_opt "--help" "Вывод этого экрана справки"
}

# Вывод отладочного сообщения
# $1 - Текст
print_debug() {
    if [[ "$DEBUG" == "1" ]]; then
        echo "DEBUG: $1" 1>&2
    fi
}

BASEDIR=$(dirname "$0")
SCRIPTDIR=$(
    cd "$BASEDIR" || exit
    pwd
)
SCRIPTNAME=$(basename "$0")
ARGSCOUNT=$#
ERRORTEXT=""
EXITCODE=0

HOSTNAME=""
HOSTUSR="_ansible"
PLAYBOOKDIR=""
KEYFILE=""
DEBUG="0"
PRINTHELP="0"
TASKS=""

while [[ -n "$1" ]]; do
    case "$1" in
    -h)
        HOSTNAME="$2"
        shift
        ;;
    -k)
        KEYFILE="$2"
        shift
        ;;
    --user)
        HOSTUSR="$2"
        shift
        ;;
    --playbook-dir)
        PLAYBOOKDIR="$2"
        shift
        ;;
    --debug)
        DEBUG="1"
        ;;
    --help)
        PRINTHELP="1"
        ;;
    *)
        ERRORTEXT="Неизвестный параметр $1"
        break
        ;;
    esac
    shift
done

# Проверяем параметры
if [[ -z "$ERRORTEXT" ]]; then
    if [[ -z "$PLAYBOOKDIR" ]]; then
        PLAYBOOKDIR="$SCRIPTDIR/playbooks"
    fi
    TASKS="$PLAYBOOKDIR/pb-gitsync.yml"
    if [[ $ARGSCOUNT -eq 0 ]]; then
        PRINTHELP="1"
    fi
    if [[ $ARGSCOUNT -eq 1 && "$PRINTHELP" == "1" ]]; then
        PRINTHELP="1"
    fi
    if [[ -z "$HOSTNAME" ]]; then
        ERRORTEXT="Необходимо задать имя хоста назначения"
    elif [[ -z "$KEYFILE" ]]; then
        ERRORTEXT="Необходимо задать расположение файла приватного ключа авторизации на целевом хосте"
    elif [[ ! -r "$KEYFILE" ]]; then
        ERRORTEXT="Не найден файл приватного ключа авторизации $KEYFILE"
    elif [[ ! -r "$TASKS" ]]; then
        ERRORTEXT="Не найден файл задач Ansible $TASKS"
    fi
fi

# Если все хорошо, то можно вызывать полезную нагрузку
if [[ -z "$ERRORTEXT" ]]; then
    ansible-playbook --inventory "$HOSTNAME," \
        --private-key "$KEYFILE" \
        --user "$HOSTUSR" \
        "$TASKS"
fi

# Завершение
if [[ "$PRINTHELP" == "1" ]]; then
	print_help "$SCRIPTNAME"
fi
if [[ -n "$ERRORTEXT" ]]; then
	print_red "$ERRORTEXT"
	print_crlf
	EXITCODE=1
fi

exit $EXITCODE
