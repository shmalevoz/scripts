#!/usr/bin/env bash

./gitlab_install.sh \
  -h localhost \
  -k /home/user/.ssh/.ansible.key \
  --user _ansible \
  --gitlab_domain gitlab.mydomain.org \
  --gitlab_redirect_http_to_https false \
  --gitlab_email_enabled true \
  --gitlab_email_from gitlab@mydomain.org \
  --gitlab_email_display_name "Gitlab notify" \
  --gitlab_email_reply_to admin@mydomain.com \
  --gitlab_smtp_enable true \
  --gitlab_smtp_address smtp.mydomain.org \
  --gitlab_smtp_port 465 \
  --gitlab_smtp_user_name "maillist" \
  --gitlab_smtp_password "Password" \
  --gitlab_smtp_domain "mydomain.org" \
  --gitlab_smtp_authentication login \
  --gitlab_smtp_enable_starttls_auto false \
  --gitlab_smtp_tls true \
  --gitlab_smtp_openssl_verify_mode peer
