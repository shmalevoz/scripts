@echo off
rem ---------------------------------------------------
rem ��⮬���᪮� ࠧ����뢠��� �����쭮� git �࠭����
rem � �ନ஢���� ��������� 䠩�� ᨭ�஭���樨
rem �࠭���� 1� � git �࠭���� ���䨣��樨
rem
rem �ॡ�� ������ 
rem		Git
rem 	OneScript ���ᨨ ��� ���� ࠢ��� 3 (५�� �� 201903)
rem
rem Valeriy Krynin aka shmalevoz
rem 2018-2021
rem ---------------------------------------------------

setlocal enabledelayedexpansion

set _ERRORTEXT=
set _RETURNCODE=0
set _SCRIPTPATH=%~dp0
call :unix_path_delims _SCRIPTPATH !_SCRIPTPATH!
call :erase_r_simbol _SCRIPTPATH !_SCRIPTPATH! /
set _SCRIPTNAME="%~nx0"
set _SCRIPTVERSION=0.0.4.11
set _PRINTHELP=
set _ENDPAUSE=1
set _DEBUG=0

rem ���� � ��㬥��� ��-� ����
:loop_parse_args_not_empty
if '%1'=='' goto loop_parse_args_end

	rem �맮� ����ணࠬ�� �஢�ન �� ����稥 ����/���祭��
	call :parse_args %1 %2
	rem ���饭�� �� ᫥���騩 ��㬥��
	shift
	rem �த������� 横��
	goto :loop_parse_args_not_empty
:loop_parse_args_end

rem �।�⠢�塞��
call :print_caption !_SCRIPTNAME!
if "!_PRINTHELP!"=="1" (
	call :print_options !_SCRIPTNAME!
	set _ENDPAUSE=
	goto :script_end
)

rem �।���⥫쭮� ������� ��६�����
rem ��� ���� ������ ��� �� ��ࠬ��஢
rem � :params2vars ��� ������� ���ࠪ⨢��
rem � :input_script_vars
rem �ᯮ��㥬�� ����� 1�
set _V8VERSION=
rem �ᯮ������� �࠭���� 1�
set _1CREPOPATH=
rem ���짮��⥫� �࠭���� 1�
set _1CREPOUSR=
rem ��஫� �࠭���� 1�
set _1CREPOPWD=
rem ��� ���७��
set _EXTNAME=
rem ����� ���㦠����� ���樨���饣� ������ �࠭���� 1�
set _EXPORTCOMMITNUM=1
rem ��⠫�� �ᯮ��塞�� 䠩��� oscript
set _OSCRIPTBIN=
rem ��⠫�� �ᯮ��塞�� 䠩��� git
set _GITBIN=
rem ���ᨬ���� ࠧ��� ��।�������� ����� git
set _GITBUFFERSIZE=1524288000
rem ��⠫�� �����쭮�� git ९������
set _GITREPODIR=
rem ��⠫�� ��室����� � ९����ਨ
set _GITSRCSUBDIR=src/config
rem ����� ���짮��⥫�� git ��-㬮�砭��
set _GITDOMAIN=
rem ����稥 業�ࠫ쭮�� �࠭���� git
set _GITSRVEXIST=
rem ���� � ���ਧ��� �� 業�ࠫ쭮� �࠭���� git
set _GITSRVURL=
set _GITSRVUSR=
set _GITSRVPWD=
rem �ப� ����㯠 � ���୥��. �।��������, �� �ப�
rem ��� http � https ���������
set _PROXYURL=
set _PROXYUSR=
set _PROXYPWD=
rem ����������� �ᯮ�짮����� ���६��⭮� ���㧪�
set _INCREMENTPOSSIBLE=
rem ��室��� 䠩�
set _OUTFILE=sync.bat
rem �ᯮ������� 䠩�� ����
set _LOGFILE=nul
rem ��⠫�� �६����� 䠩���
set _TEMPDIR=
set _TEMPLOCATED=
rem �ᯮ����⥫�� 
set _DELIMSTRING=--------------------------------------------------------
set _TEMPGUID=e3f0d7d3887e437c99588ea94ab3b938
rem �ந������ ��६����
set _GITAUTHURL=
rem ��⠫�� /usr/bin �� ���⠢�� mingw git'�
set _USRBIN=

call :print_debug "��⠫�� �ਯ� !_SCRIPTPATH!"
call :print_debug "��� 䠩�� �ਯ� !_SCRIPTNAME!"

rem --------------------------------------------------------
rem ��ॢ���� ��।���� ��ࠬ��� � ������뭥 ��६����
rem
rem �⨫���
rem

rem ᬮ��, �� ������ ��ਠ��� ࠡ�⠥�
rem �᫨ ��।��� ��� �� ���� ��ࠬ���, �
rem ��⠥�, �� �� ������ ���� ������ 
rem ��ࠬ��ࠬ� ��������� ��ப�. ���� 
rem ���짮��⥫� ������ �� ���ࠪ⨢��
set __allparams=!v8version!!confrepo!!extname!
set __allparams=!__allparams!!gitpath!!oscriptpath!
set __allparams=!__allparams!!gitrepodir!!gitsrcsubdir!!gitbuffersize!!gitdomain!!gitsrvurl!!gitsrvusr!!gitsrvpwd!!gitproxeurl!!gitproxeusr!!gitproxypwd!
set __allparams=!__allparams!!outfile!!logfile!!tempdir!

if not "!__allparams!"=="" (
	set _ENDPAUSE=
	call :params2vars 
) else (
	set _ENDPAUSE=1
	call :input_script_vars
)

rem ��ந� ���� � ��⠫��� �६����� 䠩���
if "!_TEMPDIR!"=="%TEMP%" (
	set _TEMPLOCATED=1
) else (
	set _TEMPLOCATED=
)
call :get_temp_path _TEMPDIR !_SCRIPTNAME! !_TEMPDIR!
call :print_debug "TEMPDIR {!_TEMPDIR!}"

rem ��� � �⨫�⠬ ����� ���� �� 㪠����
if "!_GITBIN!"=="" (
	call :get_util_exec_path _GITBIN !_SCRIPTPATH! git bin git.exe Git
)
if "!_OSCRIPTBIN!"=="" (
	call :get_util_exec_path _OSCRIPTBIN !_SCRIPTPATH! onescript bin oscript.exe OneScript
)
rem �஢��塞 �� ���४⭮��� ��ࠬ����
if not exist "!_OSCRIPTBIN!/oscript.exe" set _ERRORTEXT="�� ������ oscript.exe �� ��� {!_OSCRIPTBIN!}"
if not exist "!_GITBIN!/git.exe" set _ERRORTEXT="�� ������ git.exe �� ��� {!_GITBIN!}"
rem �࠭���� 1� �� �஢��塞 - ⠬ ����� ���� ��� ������� ����, ⠪ � �⥢�� �࠭����
if not exist "!_GITREPODIR!" set _ERRORTEXT="�� ������� ��⠫��� �����쭮� ����� Git ९������"
if "!_1CREPOUSR!"=="" set _ERRORTEXT="�� ����� ���짮��⥫� �࠭���� 1�"
if "!_V8VERSION!"=="" set _ERRORTEXT="�� ������ ����� ������� 1� �।�����"

rem � ��砥 �訡�� �����蠥� �믮������
if not "!_ERRORTEXT!"=="" goto :script_end

call :print_debug "��⠭������ ����� ���祭�� PATH: %PATH%"
set _USRBIN=!_GITBIN!/../usr/bin
call :print_debug "usr/bin {!_USRBIN!}"

rem ����室�� ��⠫�� �६����� 䠩���
if not exist "!_TEMPDIR!" cmd /c !_USRBIN!/mkdir -p "!_TEMPDIR!" || goto :script_end
call :print_debug "ᮧ��� ������� ��⠫�� !_TEMPDIR!"

rem ������ �ப� ����㯠 � ���୥�� �᫨ 㪠���
call :set_git_proxy !_PROXYURL! !_PROXYUSR! !_PROXYPWD!

rem �� �㤥� �ந�室��� � ��⠫��� ९������
cd /D "!_GITREPODIR!" || goto :script_end
call :print_debug "⥪�騩 ��⠫�� ������� �� ��⠫�� git ९������ %CD% (!_GITREPODIR!)"


rem -- ࠡ�稩 ��⠫�� ��室����� --
set _GITWORKDIR=!_GITREPODIR!/!_GITSRCSUBDIR!
if not exist !_GITWORKDIR! !_USRBIN!/mkdir -p !_GITWORKDIR!
call :build_auth_url _GITAUTHURL !_GITSRVURL! !_GITSRVUSR! !_GITSRVPWD!
call :print_debug "����� URL 業�ࠫ쭮�� Git ९������ !_GITAUTHURL!"

rem �� ����室����� ���樠�����㥬 ९����਩ 
call :git_repo_init !_GITREPODIR! !_GITSRCSUBDIR! !_GITBIN! !_USRBIN! !_SCRIPTNAME! !_GITBUFFERSIZE! !_OUTFILE! !_GITAUTHURL!

rem ��ந� ��ࠬ���� GitSync
rem -- gitsync --
set _GITSYNCEXEC=
call :gitsync_get_exec _GITSYNCEXEC !_OSCRIPTBIN!
set _GITSYNCCOREOPT=
call :gitsync_get_core_opt _GITSYNCCOREOPT !_V8VERSION! !_GITBIN! !_DEBUG! !_TEMPDIR!
set _repoauth=
call :gitsync_get_auth_opt _repoauth !_1CREPOUSR! !_1CREPOPWD!
set _repoext=
call :gitsync_get_extname_opt _repoext !_EXTNAME!
set _GITSYNCREPOOPT=
call :gitsync_get_1Crepo_opt _GITSYNCREPOOPT "!_repoauth!" "!_repoext!"
rem �ࠧ� ��⠭�������� ��ࠬ���� �������� 
call :print_string "����ࠨ������ GitSync...."
call :gitsync_set_plugins "!_GITSYNCEXEC!" "!_GITSYNCCOREOPT!"

rem ��ࢠ� ���㧪� �᫨ ��������� {srcsubdir}/Configuration.xml
call :print_string "���樠������ �����쭮�� Git ९������..."
call :git_init_export "!_GITSYNCEXEC!" "!_GITSYNCCOREOPT!" "!_GITSYNCREPOOPT!" !_EXPORTCOMMITNUM! !_1CREPOPATH! !_GITREPODIR! !_GITSRCSUBDIR!
rem �᫨ ��⠫��� �����-� ���⪨ ���㧪� � ��諮�� ����᪠, � �த������ �� 䨪���
call :print_string "������ ��ᯮ��஢����� ࠭�� 䠩���..."
call :git_commitpush_local_changes !_GITBIN! !_USRBIN! !_GITREPODIR! !_GITSRCSUBDIR! !_GITAUTHURL!

call :print_string "��ନ஢���� १�������饣� 䠩��..."
rem �ନ�㥬 �ਯ� ᨭ�஭���樨
rem ��᪮��� �ᯮ������ ����� ��६�����, � 
rem �� �뭮ᨬ � �⤥��� ��⮤
set _dst="!_GITREPODIR!/!_OUTFILE!"
echo @echo off> !_dst!
echo setlocal enabledelayedexpansion>>!_dst!
echo set _NAME=%%~nx0>>!_dst!
echo set _V8VERSION=!_V8VERSION!>>!_dst!
echo set _GITBIN=!_GITBIN!>>!_dst!
echo set _OSCRIPTBIN=!_OSCRIPTBIN!>>!_dst!
echo set _CONFREPO=!_1CREPOPATH!>>!_dst!
echo set _GITREPO=!_GITREPODIR!>>!_dst!
echo set _SRCDIR=!_GITREPODIR!/!_GITSRCSUBDIR!>>!_dst!
echo set _SYNCREPOOPT=!_GITSYNCREPOOPT!>>!_dst!
echo set _GITSRVURL=!_GITSRVURL!>>!_dst!
echo set _GITAUTHURL=!_GITAUTHURL!>>!_dst!
echo set _LOG=!_LOGFILE!>>!_dst!
echo set _POSTBUFFERSIZE=!_GITBUFFERSIZE!>>!_dst!
if "!_TEMPLOCATED!"=="1" (
	echo set _TEMPBASE=!_TEMPDIR!>>!_dst!
) else (
	echo set _TEMPBASE=%%TEMP%%>>!_dst!
)
echo :get_temp_next_name>>!_dst!
echo set _TEMP=%%_TEMPBASE%%\tmp%%random%%>>!_dst!
echo if exist "%%_TEMP%%" goto :get_temp_next_name>>!_dst!
echo cmd /c mkdir %%_TEMP%%>>!_dst!
echo set _SYNCOPTS=--v8version %%_V8VERSION%% --tempdir %%_TEMP%% --git-path %%_GITBIN%%/git.exe sync %%_SYNCREPOOPT%% -P -T -n 1 %%_CONFREPO%% %%_SRCDIR%% %%_GITAUTHURL%%>>!_dst!
echo rem ������ ��ࠬ���� �������� GitSync >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins init >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins enable increment >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins enable sync-remote >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins enable limit >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins disable check-authors >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins disable check-comments >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins disable smart-tags >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins disable unpackForm >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins disable tool1CD >>!_dst!
echo %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" plugins disable disable-support >>!_dst!
echo echo -------------------------------------- ^>^>%%_LOG%%>>!_dst!
echo echo %%date%% %%time%% ^>^>%%_LOG%%>>!_dst!
echo echo ���㧪� �࠭���� 1� ^>^>%%_LOG%%>>!_dst!
echo echo %%_CONFREPO%% ^>^> %%_LOG%%>>!_dst!
echo echo � ९����਩ Git ^>^>%%_LOG%%>>!_dst!
echo echo %%_GITSRVURL%% ^>^>%%_LOG%%>>!_dst!
echo cd /D "%%_GITREPO%%">>!_dst!
echo cmd /c %_GITBIN%/git.exe config core.quotepath false
echo cmd /c %_GITBIN%/git.exe checkout master -- .>>!_dst!
echo cmd /c %_GITBIN%/git.exe config http.postBuffer %%_POSTBUFFERSIZE%%>>!_dst!
echo cmd /c %%_OSCRIPTBIN%%/oscript.exe "%%_OSCRIPTBIN%%/../lib/gitsync/src/cmd/gitsync.os" %%_SYNCOPTS%%^>^>%%_LOG%% 2^>^&1 >>!_dst!
echo cmd /c rmdir /s /q %%_TEMP%%>>!_dst!
echo echo %%date%% %%time%% �����襭� ^>^>%%_LOG%%>>!_dst!
echo exit>>!_dst!

call :print_string "--------------------------------------------------------"
call :print_string "��ନ஢�� 䠩� ᨭ�஭���樨 �࠭���� 1� � Git"
call :print_string "!_dst!"
call :print_string "�ᯮ������ ��� ᨭ�஭���樨 �࠭���� 1� � ९������ Git"
call :print_string "--------------------------------------------------------"

rem �����襭�� �ਯ�
goto :script_end

rem
rem ��⮤�
rem

rem ---------------------------------------------------
rem ��ࢮ��砫�� ��ᯮ�� � git
rem %1 - ������� ����᪠ GitSync
rem %2 - �᭮��� ��樨 GitSync
rem %3 - ��樨 �࠭���� 1�
rem %4 - ����� ��ࢮ�� ���㦠����� ������
rem %5 - ���� � �࠭����� 1�
rem %6 - ��⠫�� ९������ Git
rem %7 - �����⠫�� ��室����� ���䨣��樨 � �����쭮� ����� git ९������
:git_init_export

	set __gitsync=%~1
	set __gitsynccoreopt=%~2
	set __gitsyncrepoopt=%~3
	set __1Ccommit=%4
	set __1Crepo=%5
	set __repodir=%6
	set __srcsubdir=%7

	set __srcdir=!__repodir!/!__srcsubdir!

	rem �᫨ �� ������� ���㦥����� 䠩�� ���䨣��樨, � ������ ����� ���㧪�
	if not exist "!__srcdir!/Configuration.xml" (

		call :print_debug "git_init_export: ��ࢠ� ���㧪� ���䨣��樨 � ��⠫�� ��室����� {!__srcdir!}"
		set __syncparams=!__gitsynccoreopt! sync !__gitsyncrepoopt! --limit 1 --minversion !__1Ccommit! --maxversion !__1Ccommit! !__1Crepo! !__srcdir!
		call :print_debug "git_init_export: ��ࠬ���� gitsync {!__syncparams!}"
		cmd /c "!__gitsync!" !__syncparams! || goto :script_end
		cmd /c !__gitbin!/git.exe reset HEAD~ || goto :script_end
	) else (
		call :print_debug "git_init_export: 䠩� Configuration.xml � ��⠫��� ��室����� 㦥 �������, ���樨���騩 ��ᯮ�� �� �㦥�"
	)

exit /b
rem git_init_export
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ������� ������� ��������� � ���㦠�� 
rem �� 㤠����� �ࢥ�
rem %1 - _GITBIN
rem %2 - usr/bin
rem %3 - ��⠫�� Git ९������
rem %4 - �����⠫�� ��室�����
rem %5 - url 㤠������� ९������
:git_commitpush_local_changes

	set __gitbin=%1
	set __usrbin=%2
	set __repodir=%3
	set __srcsubdir=%4
	set __repourl=%5

	cd /D !__repodir!

	:__commit_loop
	set __has_changes=
	call :git_has_local_changes __has_changes !__gitbin! !__repodir! !__srcsubdir!

	if "!__has_changes!"=="1" (
		
		call :print_debug "git_commitpush_local_changes: ��ॡ�ࠥ� ��⠫���"

		for /f "delims=" %%a in ('!__usrbin!/find !__srcsubdir! -maxdepth 1 -type d -not -path "!__srcsubdir!"') do (
			set __subdir=%%a
			set __subdir_has_changes=
			call :git_has_local_changes __subdir_has_changes !__gitbin! !__repodir! !__subdir!
			if "!__subdir_has_changes!"=="1" (
				call :git_commit_push_dir !__gitbin! !__repodir! !__subdir! !__repourl!
			) else (
				call :print_debug "git_commitpush_local_changes: ��� ��������� � �����⠫��� {!__subdir!}"
			)
		)

		rem 䨪��㥬 ������ ��⠫�� ��室�����
		call :git_commit_push_dir !__gitbin! !__repodir! !__srcsubdir! !__repourl!
	)
	
exit /b
rem git_commitpush_local_changes
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ������� � ��ࠢ��� ������� ��������� Git
rem %1 - _GITBIN
rem %2 - ��⠫�� ९������
rem %3 - 䨪��㥬� �����⠫��
rem %4 - url 㤠������� ९������
:git_commit_push_dir

	set __gitbin=%1
	set __repodir=%2
	set __subdir=%3
	set __url=%4

	cd /D !__repodir!

	call :print_string "gitbin {!__gitbin!}"

	call :print_debug "git_commit_push_dir: 䨪��㥬 ��������� {!__subdir!}"
	cmd /c !__gitbin!/git.exe add !__subdir! || goto :script_end
	cmd /c !__gitbin!/git.exe commit -m "Init commit !__subdir!" || goto :script_end
	if not "!__url!"=="" (
		cmd /c "!__gitbin!/git.exe" push origin master || goto :script_end
	)

exit /b
rem git_commit_push_dir
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� 1 �� ����稨 ��������� 
rem � �����쭮� ����� Git
rem %1 - ��� १�������饩 ��६�����
rem %2 - _GITBIN
rem %3 - ��⠫�� ९������
rem %4 - �����⠫��
:git_has_local_changes

	set __result=
	set __gitbin=%2
	set __repodir=%3
	set __subdir=%4

	cd /D !__repodir!

	for /f "delims=" %%a in ('cmd /c "!__gitbin!/git.exe" status -s !__subdir!') do (
		set __result=1
		goto :__git_has_local_changes_verify_end
	)
	:__git_has_local_changes_verify_end

	if "!__result!"=="1" (
		call :print_debug "git_has_local_changes: ���� ��������� � ��⠫��� {!__repodir!/!__subdir!}"
	) else (
		call :print_debug "git_has_local_changes: ��� ��������� � ९����ਨ {!__repodir!/!__subdir!}"
	)

	set %1=!__result!

exit /b
rem git_has_local_changes
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ���樠������ git ९������
rem %1 - ��⠫�� ९������
rem %2 - �����⠫�� ��室�����
rem %3 - _GITBIN
rem %4 - _USRBIN
rem %5 - _SCRIPTNAME
rem %6 - ࠧ��� ���� ��ࠢ�� Git
rem %7 - ���ਧ������ URL 業�ࠫ쭮�� �࠭����
rem %8 - ��� ����筮�� 䠩�� 
:git_repo_init
	
	set __repodir=%1
	set __srcsubdir=%2
	set __gitbin=%3
	set __usrbin=%4
	set __scriptname=%5
	set __gitbuffersize=%6
	set __outfile=%7
	set __remoterepo=%8
	set __srcdir=!__repodir!/!__srcsubdir!

	rem �� �믮������ � ��⠫��� ९������ git
	cd /D "!__repodir!"

	rem �᫨ ��⠫�� ����, � ���樠�����㥬 ᢥ��� git ९����਩
	if not exist "!__repodir!/.git" (

		call :print_debug "git_repo_init: ���樠�����㥬 ���� ������� ९����਩"
		set __remotehascontent=
		if not "!__remoterepo!"=="" (
			for /f "tokens=1* delims=" %%a in ('cmd /c "!__gitbin!/git.exe" ls-remote !__remoterepo!') do (
				set __remotehascontent=1
			)
		)

		call :print_debug "git_repo_init: ��।����� ����稥 ᮤ�ন���� � 㤠������ ९����ਨ {!__remotehascontent!}"

		rem �᫨ �� ������� ९����਩ ���
		rem 㤠����� ९������ ����, � ����室��� 
		rem ��ࢮ��砫쭮 ���樠����஢��� �������
		rem �� ����稨 㤠������� �� ���� �易��
		rem ����� ᮡ��
		set __needinit=
		if "!__remotehascontent!"=="" (
			set __needinit=1
		)
		if "!__remoterepo!"=="" (
			set __needinit=1
		)
		if "!__needinit!"=="1" (
			call :print_debug "git_repo_init: ���樠�����㥬 �������� �����"
			cmd /c !__gitbin!/git.exe init || goto :script_end
		)
		
		rem �᫨ �� ����� ���譨�, � ���樨�㥬 ���⮩ �������
		rem ���� ������㥬 � �� �뫮 �� ᬥ頥��� �� ����� ����
		if not "!__remoterepo!"=="" (
			if not "!__remotehascontent!"=="1" (
				call :print_debug "git_repo_init: ������塞 ��� � 㤠����� ९����ਥ� � ���⮩ �������"
				cmd /c !__gitbin!/git.exe remote add origin !__remoterepo! || goto :script_end
			) else (
				call :print_debug "git_repo_init: ������㥬 �����⮩ 㤠����� ९����਩"
				rem �ﭥ� ⮫쪮 master � ⮫쪮 ��᫥���� ������
				cmd /c !__usrbin!/rm.exe -rf !__repodir!
				cmd /c !__gitbin!/git.exe clone -b master --depth 1 !__remoterepo! !__repodir! || goto :script_end
			)
		)
		call :print_debug "git_repo_init: ��⠭�������� ��६���� git ९������"
		cmd /c !__gitbin!/git.exe config user.name "!__scriptname!" || goto :script_end
		cmd /c !__gitbin!/git.exe config user.email "noreply@localhost" || goto :script_end
		cmd /c !__gitbin!/git.exe config core.quotepath false || goto :script_end
		cmd /c !__gitbin!/git.exe config core.autocrlf false || goto :script_end
		cmd /c !__gitbin!/git.exe config http.postBuffer !__gitbuffersize! || goto :script_end
	) else (
		call :print_debug "git_repo_init: ������� ९����਩ 㦥 ���樠����஢��"
	)

	set __modified=0

	rem ? ��������� .gitignore
	if not exist "!__repodir!/.gitignore" (
		call :print_debug "git_repo_init: �ନ�㥬 䠩� .gitignore"
		echo !__outfile! > !__repodir!/.gitignore
		echo /!__srcsubdir!/AUTHORS >> !__repodir!/.gitignore
		echo /!__srcsubdir!/ConfigDumpInfo.xml >> !__repodir!/.gitignore
		echo /!__srcsubdir!/Ext/ParentConfigurations/* >> !__repodir!/.gitignore
		set __modified=1
	)

	rem ��⠫�� ��室�����, ��᪨ �᪫�祭�� ��� ���� � ��易⥫�� 䠩�� � ���
	if not exist "!__srcdir!/VERSION" (
		rem ���㦥���� ����� ��⠭�������� � ����
		call :git_set_conf_version !__repodir! !__srcsubdir! 0
		set __modified=1
	)
	
	if "!__modified!"=="1" (
		call :git_commit_push_dir !__gitbin! !__repodir! !__srcsubdir! !__remoterepo!
		call :git_commit_push_dir !__gitbin! !__repodir! . !__remoterepo!
	)

	rem ��� AUTHORS modified �� ��⠭��������, ��� � �᪫�祭���
	if not exist "!__srcdir!/AUTHORS" (
		call :print_debug "git_repo_init: ᮧ���� AUTHORS � ��⠫��� ��室�����"
		type nul > !__srcdir!/AUTHORS
	)
	
exit /b	
rem git_repo_init
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��⠭���� ���㦠���� ���ᨨ ���䨣��樨
rem %1 - ��⠫�� ९������
rem %2 - �����⠫�� ��室�����
rem %3 - �����
:git_set_conf_version

	set __repodir=%1
	set __srcsubdir=%2
	set __version=%3
	set __dstdir=!__repodir!/!__srcsubdir!

	call :print_debug "��⠭���������� ����� ���㦥���� ���䨣��樨 !__version!, ��⠫�� ��室����� {!__dstdir!}"

	echo ^<?xml version="1.0" encoding="UTF-8"?^> > !__dstdir!/VERSION
	echo ^<VERSION^>!__version!^</VERSION^> >> !__dstdir!/VERSION

exit /b
rem git_set_conf_version
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��ᢠ����� %1 ��६����� ��������� ���祭��
rem �뢮�� ���᪠��� %2, �� ���⮬ ����� 
rem ��ᢠ����� ���祭�� �� 㬮�砭�� %3
rem %1 - ��� ���������饩 ��६�����
rem %2 - ⥪�� �����
rem %3 - 㬮�砭��
:input_var

	set __input=
	set /P __input=%2
	if "!__input!"=="" (
		set %1=%3
	) else (
		set %1=!__input!
	)

exit /b
rem input_var
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ���� ��⠫��� � ��६����� %1. �����頥� ��⠫��
rem ��� �������饣� ࠧ����⥫�
rem %1 - ��� १�������饩 ��६�����
rem %2 - ���᪠���
rem %3 - ���祭�� ��-㬮�砭��
:input_path

	set __input=
	set /P __input=%2
	if "!__input!"=="" (
		set %1=%3
	) else (
		call :erase_r_simbol __input !__input! \
		call :unix_path_delims __input !__input!
		set %1=!__input!
	)

exit /b
rem input_path
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �뢮� ���ଠ樨 � �ਯ�
rem %1 - ��� �ਯ�
:print_caption

	call :print_string "----------------------------------------------------"
	call :print_string "��⮬���᪮� ࠧ����뢠��� �����쭮� git �࠭����"
	call :print_string "� �ନ஢���� ���������� 䠩�� ᨭ�஭���樨"
	call :print_string "�࠭���� 1� � git �࠭���� ���䨣��樨 ���ᨨ !SCRIPTVERSION!"
	call :print_string "� ������ ���祭��, 㪠����� � �������� ᪮���� [] ����� ���祭�ﬨ ��-㬮�砭��"
	call :print_string "�ᯮ�짮�����:"
	call :print_string "%~1 [--���� ���祭�� [--���� ���祭�� [...]]]"
	call :print_string "�� ������⢨� ��易⥫��� ��権 ���� ������ ���ࠪ⨢�� ������"
	call :print_string "��� �뢮�� �ࠢ�� �믮����"
	call :print_string "%~1 --help"
	call :print_string "��������! ��ਯ� ������ ��室���� �� �����쭮� ��᪥. �� �ᯮ������� �� �⥢�� ��᪠� �㤥� �訡��!"
	call :print_string "��������! �� ��� ����室��� ������� � �ᯮ�짮������ ࠧ����⥫� '/'"
	call :print_empty_string

exit /b
rem print_caption
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �뢮� ��ࠢ�� �� ����
rem %1 - ��� �ਯ�
:print_options

	call :print_string "��樨:"
	call :print_string "  1C"
	call :print_string "    --v8version - ����� �ᯮ��㥬�� ������� 1� ���� X.X.X.X"
	call :print_string "    --repopath - �ᯮ������� �࠭���� 1�"
	call :print_string "    --repousr - ���짮��⥫� �࠭���� 1�"
	call :print_string "    --repopwd - ��஫� ���짮��⥫� �࠭���� 1�"
	call :print_string "    --extname - ��� ���७�� �࠭�饣��� � �࠭����. �� ������⢨� ��⠥���, �� � �࠭���� ���䨣����"
	call :print_string "    --commitnum - ����� ��砫쭮�� ������ �� ��ࢮ� ���㧪� �࠭���� � Git"
	call :print_string "  �⨫���"
	call :print_string "    --gitpath - ��⠫�� ��⠭���� Git"
	call :print_string "    --oscriptpath - ��⠫�� ��⠭���� OneScript"
	call :print_string "  Git"
	call :print_string "    --gitrepodir - ��⠫�� �����쭮� ����� Git ९������"
	call :print_string "    --gitsrcsubdir - �����⠫�� �࠭���� ��室����� ���䨣��樨 � Git ९����ਨ"
	call :print_string "    --gitbuffersize - ���ᨬ���� ࠧ��� ��।������� �� ࠧ ������ Git. ��-㬮�砭�� 1524288000"
	call :print_string "    --gitdomain - ����� ���짮��⥫�� Git ��-㬮�砭��"
	call :print_string "    --gitsrvurl - URL �ࢥ� �࠭���� Git, �� ������⢨� �㤥� ᮧ���� �����쭮� �࠭���� Git"
	call :print_string "    --gitsrvusr - ���짮��⥫� �ࢥ� �࠭���� Git"
	call :print_string "    --gitsrvpwd - ��஫� ���짮��⥫� �ࢥ� �࠭���� Git"
	call :print_string "    --gitproxyurl - URL �ப� ����㯠 Git � ���୥�"
	call :print_string "    --gitproxyusr - ���짮��⥫� �ப� ����㯠 Git � ���୥�"
	call :print_string "    --gitproxypwd - ��஫� �ப� ����㯠 Git � ���୥�"
	call :print_string "  ������ 䠩� ᨭ�஭���樨"
	call :print_string "    --outfile - ��� ����筮�� 䠩�� ᨭ�஭���樨, ��-㬮�砭�� sync.bat, �㤥� ᮧ��� � ��⠫��� ९������ Git"
	call :print_string "    --logfile - ������ ��� � ��⥬ 䠩�� ���� ᨭ�஭���樨, �� ������⢨� ��� �⪫�祭"
	call :print_empty_string

exit /b
rem print_options
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ����� �ࠢ��
rem %1 - ��� �ਯ�
rem %2 - ⥪�� �訡��
:print_error

	set __error=%~2
	if not "!__error!"=="" (
		call :print_string "----------------------------------------------------"
		call :print_string "�訡��:"
		call :print_string "!__error!"
	)

exit /b
rem print_error
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �뢮��� �� ��࠭ ��ப� �᪫��� ��ࠬ���騥 ����窨
rem %1 - ���⠥��� ��ப�
:print_string

	call :unquote __msg %*
	echo !__msg!

exit /b
rem print_string
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �뢮� �⫠��筮�� ᮮ�饭��
rem %1 - ⥪�� ᮮ�饭��
:print_debug

	if not "!_DEBUG!"=="0" (
		if not "!_DEBUG!"=="" call :print_string "DEBUG: %~1"
	)

exit /b
rem print_debug
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �뢮� ���⮩ ��ப�
:print_empty_string

	echo.

exit /b	
rem print_empty_string
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ���ࠥ� ��ࠬ���騥 ����窨
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��ப� ��ࠬ������ ����窠��
:unquote

	set %1=%~2

exit /b
rem unquote
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �������� �������饣� ᨬ���� �� ��ப� 
rem �� ��� ����稨
rem %1 - ��� ��६�����
rem %2 - �����塞�� ��ப�
rem %3 - �������騩 ᨬ���
:erase_r_simbol

	set __str=%2
	set __char=%__str:~-1%
 	if "!__char!"=="%3" (
 		set __substr=%__str:~0,-1%
		set %1=%__str:~0,-1%
	) else (
		set %1=!__str!
	)

exit /b
rem erase_r_simbol
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��ॢ���� ���祭�� ��६����� � ���孨� ॣ����
rem %1 - ��� ��६�����, ᮤ�ঠ饩 ��ப�
:string_toupper

	for %%L IN (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO SET %1=!%1:%%L=%%L!

exit /b
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��ॢ���� ���祭�� ��६����� � ������ ॣ����
rem %1 - ��� ��६�����, ᮤ�ঠ饩 ��ப�
:string_tolower

	for %%L IN (a b c d e f g h i j k l m n o p q r s t u v w x y z) DO SET %1=!%1:%%L=%%L!

exit /b
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� � RESULT url � ������� HTTP ���ਧ�樥�
rem %1 - ��� १�������饩 ��६�����
rem %2 - ���� �����
rem %3 - ��� ���짮��⥫�
rem %4 - ��஫�
:build_auth_url

	set __result=
	set __baseurl=%2
	set __usr=%3
	set __pwd=%4
	set __protocol=
	set __url=

	if not "!__baseurl!"=="" (
		for /f "tokens=1* delims=://" %%a in ("!__baseurl!") do (
			set __protocol=%%a
			set __url=%%b
		)
		set __result=!__protocol!://!__usr!:!__pwd!@!__url!
	)

	set %1=!__result!

exit /b
rem build_auth_url
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ���� � ᯮ��塞��� 䠩��
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��� �ᯮ��塞��� 䠩��
:get_exec_path
	
	rem �।���⥫쭮 ��⨬ १����
	set %1=
	set __result=

	call :print_debug "get_exec_path: �஢�ઠ ���������� �믮������ %2 �� ����㯭��� � ��६����� PATH"
	set "__temp_full_path=%~$PATH:2"
	call :print_debug "get_exec_path: ����� ���� !__temp_full_path!"
	if not "!__temp_full_path!"=="" (
		call :full_name_folder __folder !__temp_full_path!
		call :print_debug "get_exec_path: ��⠫�� �� ������� ��� !__folder!"
		call :erase_r_simbol __folder_clear !__folder! \
		call :unix_path_delims __result !__folder_clear!
		set %1=!__result!
	)
	call :print_debug "get_exec_path: �����頥� � ��७��� %1 !__result! "

exit /b
rem get_exec_path
rem ---------------------------------------------------

rem ---------------------------------------------------
rem %1 - ��� ����筮� ��६�����
rem %2 - ��� ��⠫��� �ਯ�
rem %3 - ��� �����⠫��� �⨫��� � ��⠫��� �ਯ�
rem %4 - ��� �����⠫��� bin ��⠫��� ��⠭����
rem %5 - ��� �ᯮ��塞��� 䠩��
rem %6 - ��� �ணࠬ��
:get_util_exec_path

	set __result=

	set __script_path=%2
	set __util_subdir=%3
	set __bin_dir=
	if not "%4"=="" set __bin_dir=/%4
	set __exec_name=%5
	set __exec_desc=%6

	call :erase_r_simbol __temp !__param_path! \
	call :unix_path_delims __exec_path !__temp!!__bin_dir!

	rem �饬 �� PATH
	call :get_exec_path __exec_path !__exec_name!
	if not "!__exec_path!"=="" (
		set __result=!__exec_path!
		call :print_debug "!__exec_name! ������ �� ��६����� PATH {!__result!}"
	) else (
		rem �饬 � ��⠫��� �ਯ�
		set __exec_path=!__script_path!/!__util_subdir!!__bin_dir!
		if exist "!__exec_path!/!__exec_name!" (
			call :print_debug "������ !__exec_name! � ��⠫��� �ਯ� {!__exec_path!}"
			set __result=!__exec_path!
		)
	)
	call :erase_r_simbol __result !__result! \
	call :unix_path_delims __result !__result!

	if not exist "!__result!/!__exec_name!" (
		set __result=
	)

	set %1=!__result!

exit /b
rem get_input_exec_path
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��⠫�� �� ������� ����� 䠩��
rem %1 - ��� १�������饩 ��६�����
rem %2 - ������ ��� 䠩��
:full_name_folder
	
	set __temp=%~dp2
	set %1=!__temp!

exit /b
rem full_name_folder
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ������ ��㬥�⮢ ��������� ��ப�
rem �᫨ ��㬥�� 1 ��稭����� �� -- � ��६�����
rem � ������ ��㬥�� 1 ��ᢠ������� ���祭��
rem ��㬥�� 2
rem %1 - ��㬥�� ��������� ��ப�
rem %2 - ��㬥�� ��������� ��ப�
:parse_args

	set __prefix=%1
	set __prefix=%__prefix:~0,2%
	if "%__prefix%"=="--" (
		for /f "tokens=1* delims=--" %%a in ("%1") do (
			set %%a=%2
			set __temp=%%a
			call :string_toupper __temp
			if "!__temp!"=="HELP" set _PRINTHELP=1
			if "!__temp!"=="DEBUG" set _DEBUG=1
		)
	)

exit /b
rem parse_args
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��� �६������ 䠩��
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��� �ਯ�
rem %3 - ��� �������� ��⠫���
:get_temp_path

	set __base=%3
	if "!__base!"=="" set __base=%TEMP%

	:get_temp_path_next_name
	set __temp_name=!__base!\tmp%random%
	if exist "!__temp_name!" goto :get_temp_path_next_name
	call :unix_path_delims __temp_name !__temp_name!

	set %1=!__temp_name!

exit /b
rem get_temp_path
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ���� � ࠧ����⥫ﬨ �⨫� unix
rem %1 - ��� ��६����� �ਥ�����
rem %2 - ��ப� ��� 
:unix_path_delims

	set __replace_temp=%2
	set %1=!__replace_temp:\=/!

exit /b
rem unix_path_delims
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ���� � ࠧ����⥫ﬨ �⨫� windows
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��ப� ���
:windows_path_delims

	set __replace_temp=%2
	set %1=!__replace_temp:/=\!

exit /b
rem windows_path_delims
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �ࠢ����� ��ப ���ᨩ. �����頥� 
rem -1 == %2 < %3
rem 0 == %2 = %3
rem 1 == %2 > %3
rem �।���������� ���ᨨ ���� X.X.X.X
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��ப� ���ᨨ 1
rem %3 - ��ப� ���ᨨ 2
:compare_versions

	if not "!DEBUG!"=="0" call :print_string "DEBUG: compare_versions: �ࠢ����� ���ᨩ %2 � %3"

	set __result=0

	set __ver1=%2
	set __ver2=%3

	for /f "tokens=1-4 delims=." %%c in ("!__ver1!") do (
		set __ver1_1=%%c
		set __ver1_2=%%d
		set __ver1_3=%%e
		set __ver1_4=%%f
	)
	for /f "tokens=1-4 delims=." %%c in ("!__ver2!") do (
		set __ver2_1=%%c
		set __ver2_2=%%d
		set __ver2_3=%%e
		set __ver2_4=%%f
	)
	call :print_debug "compare_versions: ��� ��ࢮ� ���ᨨ: !__ver1_1! !__ver1_2! !__ver1_3! !__ver1_4!"
	call :print_debug "compare_versions: ��� ��ன ���ᨨ: !__ver2_1! !__ver2_2! !__ver2_3! !__ver2_4!"
	for /l %%a in (1,1,4) do (
		if !__ver1_%%a! GTR !__ver2_%%a! (
			set __result=1
			goto :compare_versions_end
		)
		if !__ver1_%%a! LSS !__ver2_%%a! (
			set __result=-1
			goto :compare_versions_end
		)
	)

	:compare_versions_end
	call :print_debug "compare_versions: १���� �ࠢ����� ���ᨩ {!__ver1!} � {!__ver2!} ࠢ�� {!__result!}"
	set %1=!_result!

exit /b	
rem compare_versions
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �८�ࠧ�� ��।���� ��ࠬ���� � 
rem ������� ��६����
rem ��⠭�������� ⥪�� �訡�� � ��砥 ������⢨�
rem ��易⥫��� ��ࠬ��஢
:params2vars

	call :set_param2var _GITBUFFERSIZE gitbuffersize 0
	call :set_param2var _GITDOMAIN gitdomain 0
	call :set_param2var _GITSRVURL gitsrvurl 0
	call :set_param2var _GITSRVUSR gitsrvusr 0
	call :set_param2var _GITSRVPWD gitsrvpwd 0
	call :set_param2var _PROXYURL gitproxyurl 0
	call :set_param2var _PROXYUSR gitproxyusr 0
	call :set_param2var _PROXYPWD gitproxypwd 0
	call :set_param2var _EXTNAME extname 0
	call :set_param2var _1CREPOPWD repopwd 0
	call :set_param2var _GITBIN gitpath 0
	call :set_param2var _OSCRIPTBIN oscriptpath 0
	call :set_param2var _GITSRCSUBDIR gitsrcsubdir 0
	call :set_param2var _EXPORTCOMMITNUM commitnum 0
	call :set_param2var _TEMPDIR tempdir 0
	call :set_param2var _OUTFILE outfile 0
	call :set_param2var _LOGFILE logfile 0

	call :set_param2var _GITREPODIR gitrepodir 1
	call :set_param2var _1CREPOUSR repousr 1
	call :set_param2var _1CREPOPATH repopath 1
	call :set_param2var _V8VERSION v8version 1

	call :path_add_subdir _GITBIN !_GITBIN! bin
	call :path_add_subdir _OSCRIPTBIN !_OSCRIPTBIN! bin

exit /b
rem params2vars
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��ᢠ����� ���祭�� ��ࠬ��� �����쭮� ��६����� 
rem %1 - ��� �����쭮� ��६�����
rem %2 - ��� ��ࠬ���
rem %3 - ��易⥫쭮��� ��ࠬ���
:set_param2var

	set __temp=!%2!
	if not "!__temp!"=="" (
		call :print_debug "��⠭���� ��६����� %1 ���祭��� {!__temp!}"
		set %1=!__temp!
	) else (
		if "%3"=="1" (
			call :print_debug "��⠭���� �訡�� ������⢨� ��易⥫쭮�� ��ࠬ��� %2"
			set _ERRORTEXT="�� ����� ��ࠬ��� %2"
		)
	)

exit /b
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ���ࠪ⨢�� ���� ��६����� �ਯ�
:input_script_vars

	call :input_var _V8VERSION "������ ����� �ᯮ��㥬�� ���ᨨ 1� (���ਬ�� 8.3.10.2466): "
	call :input_path _GITBIN "������ ���� � ��⠫��� ��⠭���� GIT: "
	call :input_path _OSCRIPTBIN "������ ���� � ��⠫��� ��⠭���� OneScript: "
	call :input_path _1CREPOPATH "������ ���� � �࠭����� 1�: "
	call :input_var _1CREPOUSR "������ ���짮��⥫� �࠭���� 1�: "
	call :input_var _1CREPOPWD "������ ��஫� ���짮��⥫� �࠭���� 1�: "
	rem � �࠭���� ���७�� ��� ���䨣����?
	call :input_var _EXTNAME "�࠭���� ᮤ�ন� ���७�� ���䨣��樨? Y/N [N]: " N
	call :string_toupper _EXTNAME
	if not "!_EXTNAME!"=="N" (
		call :input_var _EXTNAME "������ ��� ���७��: "
	) else (
		set _EXTNAME=
	)
	call :input_var _EXPORTCOMMITNUM "������ ����� ��ࢮ�� ���㦠����� ������ [1]: " 1
	call :input_path _GITREPODIR "������ ��⠫�� �����쭮� ����� �࠭���� Git: "
	call :input_path _GITSRCSUBDIR "������ �����⠫�� �࠭���� ��室��� 䠩��� � �࠭���� Git [src/config]: " src/config
	call :input_var _GITDOMAIN "������ ����� ���짮��⥫�� git ��-㬮�砭��: " 
	call :input_var __answer "������� 業�ࠫ쭮� Git �࠭����? Y/N [Y]" Y
	call :string_toupper __answer
	if "!__answer!"=="Y" (
		call :input_var _GITBUFFERSIZE "������ ���ᨬ���� ࠧ��� ����� ��ࠢ�� git [!_GITBUFFERSIZE!]: " !GITBUFFERSIZE!
		call :input_var _GITSRVURL "������ URL ��ࠫ쭮�� git �࠭���� �஥��: "
		call :input_var _GITSRVUSR "������ ��� ���짮��⥫� ��� ����� ��������� � �࠭���� git [commiter]: " commiter
		call :input_var _GITSRVPWD "������ ��஫� ���짮��⥫� �࠭���� git : "
		if not exist "%USERPROFILE%/.gitconfig" (
			call :input_var _PROXYURL "�᫨ �ᯮ������ ������ ���� http(s) �ப� []: "
			if not "!_PROXYURL!"=="" (
				call :input_var _PROXYUSR "�� ���ਧ�樨 �� �ப�-�ࢥ�, ���짮��⥫� []: "
				call :input_var _PROXYPWD "�� ���ਧ�樨 �� �ப�-�ࢥ�, ��஫� []: "
			)
		)
	)
	
	call :input_path _TEMPDIR "������ ��⠫�� �६����� 䠩���: [%TEMP%] " %TEMP%
	call :input_path _OUTFILE "������ ��� ᮧ��������� 䠩�� ᨭ�஭���樨 [sync.bat]: " sync.bat
	call :input_path _LOGFILE "������ ������ ��� (� ��⥬) 䠩�� ���� ᨭ�஭���樨 [nul]: " nul

	call :path_add_subdir _GITBIN !_GITBIN! bin
	call :path_add_subdir _OSCRIPTBIN !_OSCRIPTBIN! bin

exit /b
rem input_script_vars
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �������� ���� �����⠫���� � ��砥 �� ���⮣� ���
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��⠫��
rem %3 - ��� ����⠫���
:path_add_subdir

	set __result=%2
	set __subdir=%3
	if not "!__result!"=="" set __result=!__result!/!__subdir!
	call :print_debug "��� ��६����� ��� %1 �������� �����⠫�� !__subdir!, १���� {!__result!}"

	set %1=!__result!

exit /b
rem bin_path_complete
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��⠭�������� proxy ����㯠 git � ���୥��
rem %1 - URL
rem %2 - ���짮��⥫�
rem %3 - ��஫�
:set_git_proxy

	set __proxyurl=%1
	set __proxyusr=%2
	set __proxypwd=%3

	if not "!__proxyurl!"=="" (
		set __proxy=!__proxyurl!
		if not "!__proxyusr!"=="" (
			call :build_auth_url __proxy !__proxyurl! !__proxyusr! !__proxypwd!
		)
		git.exe config --global http.proxy !__proxy!
		git.exe config --global https.proxy !__proxy!
		call :print_debug "����� proxy ����㯠 Git � ���୥� {!__proxy!}"
	)

exit /b
rem set_git_proxy
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� �᭮��� ��ࠬ���� GitSync
rem %1 - ��� १�������饩 ��६�����
rem %2 - ����� ������� 1�
rem %3 - _GITBIN
rem %4 - �ਧ��� �⫠���
rem %5 - ���� � �६������ ��⠫���
:gitsync_get_core_opt

	set __v8version=%2
	set __gitbin=%3
	set __debug=%4
	set __tmp=%5

	set __result=--v8version !__v8version! --git-path !__gitbin!/git.exe
	if not "!__debug!"=="0" set __result=!__result! --verbose
	set __result=!__result! --tempdir !__tmp!
	call :print_debug "gitsync_get_core_params: �᭮��� ��ࠬ���� GitSync {!__result!}"

	set %1=!__result!

exit /b
rem gitsync_get_core_opt
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��ࠬ��� ��⥭䨪�樨 GitSync �
rem �࠭���� 1� ��ࠬ����� ����窠��
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��� ���짮��⥫� �࠭����
rem %3 - ��஫� ���짮��⥫� �࠭����
:gitsync_get_auth_opt

	set __usr=%2
	set __pwd=%3

	set __result=--storage-user !__usr!
	if not "!__pwd!"=="" set __result=!__result! --storage-pwd !__pwd!
	call :print_debug "gitsync_get_auth_params: ��ࠬ���� ���ਧ�樨 � �࠭���� 1� {!__result!}"

	set %1=!__result!

exit /b
rem getsync_get_auth_opt
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��ࠬ��� ����� ���७�� 
rem �࠭�饬�� � �࠭���� 1� ��� GitSync
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��� ���७��
:gitsync_get_extname_opt

	set __result=
	set __ext=%2

	if not "!__ext!"=="" set __result=--extension !__ext!
	call :print_debug "gitsync_get_extname_opt: ��ࠬ��� ����� ���७�� ��� GitSync {!__result!}"

	set %1=!__result!

exit /b
rem gitsync_get_extname_opt
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ��樨 ������祭�� GitSync
rem � �࠭����� 1�
rem %1 - ��� १�������饩 ��६�����
rem %2 - ��樨 ���ਧ�樨
rem %3 - ��樨 �࠭���� ���७��
:gitsync_get_1Crepo_opt

	set __result=
	set __auth=%~2
	set __ext=%~3

	set __result=!__auth!
	if not "!__ext!"=="" set __result=!__result! !__ext!
	call :print_debug "gitsync_get_1Crepo_opt: ��樨 ������祭�� GitSync � �࠭����� 1� {!__result!}"

	set %1=!__result!

exit /b
rem gitsync_get_1Crepo_opt
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ��⠭���� ��ࠬ��஢ �������� gitsync
rem %1 - ������� ����᪠ gitsync
rem %2 - ������ ��ࠬ���� gitsync
:gitsync_set_plugins

	set __exec=%1
	set __coreparams=%~2

	cmd /c !__exec! !__coreparams! plugins init
	cmd /c !__exec! !__coreparams! plugins enable increment
	cmd /c !__exec! !__coreparams! plugins enable sync-remote
	cmd /c !__exec! !__coreparams! plugins enable limit
	cmd /c !__exec! !__coreparams! plugins disable check-authors
	cmd /c !__exec! !__coreparams! plugins disable check-comments
	cmd /c !__exec! !__coreparams! plugins disable smart-tags
	cmd /c !__exec! !__coreparams! plugins disable unpackForm
	cmd /c !__exec! !__coreparams! plugins disable tool1CD
	cmd /c !__exec! !__coreparams! plugins disable disable-support

	if not "!_DEBUG!"=="0" (
		cmd /c !__exec! plugins list
	)

exit /b
rem gitsync_set_plugins
rem ---------------------------------------------------

rem ---------------------------------------------------
rem �����頥� ������� ����᪠ gitsync
rem %1 - ��� ��६����� �����頥���� ���祭��
rem %2 - ��⠫�� bin onescript
:gitsync_get_exec

	set __oscript=%2
	set __result=!__oscript!/oscript.exe !__oscript!/../lib/gitsync/src/cmd/gitsync.os
	call :print_debug "gitsync_get_exec: ������� ����᪠ gitsync {!__result!}"

	set %1=!__result!

exit /b
rem gitsync_get_exec
rem ---------------------------------------------------

rem ---------------------------------------------------
rem ����� �ਯ�
:script_end
if ERRORLEVEL 1 (
	if "!_ERRORTEXT!"=="" set _ERRORTEXT="�ந��諠 �訡�� �� �믮������ �������..."
	set _RETURNCODE=1
)
if not "!_ERRORTEXT!"=="" (
	call :print_error !_SCRIPTNAME! !_ERRORTEXT!
) else (
	call :print_string "��ਯ� �ᯥ譮 �믮����"
)
rem ��⨬ �� ᮡ��
if exist "!_TEMPDIR!" rmdir /s /q "!_TEMPDIR!"

pause
exit !_RETURNCODE!
